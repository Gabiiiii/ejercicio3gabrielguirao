package ejercicio3;

import java.util.Scanner;

public class Ejercicio3 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce una cadena");
		String cadena=input.nextLine();
		System.out.println("La cadena en minusculas es:");
		System.out.println(cadena.toLowerCase());
		input.close();

	}

}
